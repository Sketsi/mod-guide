**Priorities & Professionality:**

1. Be friendly

2. Be involved

3. Be professional

4. Be. Professional. – Yes, this is listed twice.
 
Be friendly: This above all. Say hey to new players and ask if they have any questions. Say hey to returning players when they sign back in. Join in the conversation in public chats and answer questions freely. When you are friendly with the players, they want to stick around and feel like they belong. The longer they stick around and the more they feel that this is “their” server, the more likely they are to donate. The more likely they are to donate, the longer we can keep being an awesome server (servers cost money).
 
Being involved: If a player is proud of something they’ve built or done, ask to see it. If a player would like an event (spleef, obstacle courses, wither fights, whatever), run it. Build stuff or consider helping other players build (but make sure you don’t give away free items – that’s an unfair advantage!). It’s important to give players a reason to come back!
Being professional is the hardest part of being staff. Many of our players are young, and the ones who aren’t young, some of them are trolls. One trait that young players and trolls have in common? They are absolutely certain that they shouldn’t be in trouble, and when someone says they are, they either get upset or mad. If they decide they don’t like you, they’ll find reasons to make that known in as public a way as they can. All while they continue to expect you to save them from rule-breakers. It can be a challenge to stay nice and professional with these ne’er-do-wells, but that is a challenge you must meet head-on. If you’re having trouble dealing with someone, ask another staff member to sub in for you in staff chat.
 
Being professional isn’t just about how you deal with problem players. It’s also in how you interact with your fellow staff. We are a diverse group of people, with different personalities and interests. You’re bound to butt heads with someone eventually. DO NOT EVER do so on the server in any public chat. You are to try to work out the problem with the person directly first, in staff chat if you need a second opinion or using a senior staff member as a mediator if neither of those two options work. If you are incapable or unwilling to follow these simple guidelines, you may wish to ask an admin to demote you. We don’t mind having you play on our servers, but we have to make sure we have only the best people on staff.
 
**Economy:** As a Mod, you now have access to creative </gm 1> and spectator mode </gm 3>. These commands will assist you in repairing grief, player investigations, etc. However, despite their benefits, you are no longer allowed to interact with the economy. This means you cannot buy/sell items or give away items, even if you attained them legally and through legitimate play. Failure to abide by this guideline will result in termination of your position.

**Reinstating Pwarps/Homes:** When players change their name, their balance, pwarps, and /home are wiped. You have the ability to resinstate the locations of player's pwarps & /home:

* /home <player:home> Teleports you to a specific player's home.

* /sethome <player:home> Sets the home of a specific player.

* /pwarp <player> 0-5 Teleports you to a specific player's specific personal warp. 

* /setpwarp <player> 0-5 Sets a specific personal warp of a specific player.

*You do NOT have the ability to reinstate balances, leave the task for an Admin. 

**CoreProtect:** Coreprotect is our block logging/grief repairing plugin. This is the system you will use when responding to grief, confirming ownership, reinstating stolen items, and a variety of other things, so this is one you want to be familiar with.
 
/co inspect (/co i) This command activates 'inspect' mode. You can bind this tool to any block and it will allow you to inspect the placements/destructions/uses of any block.
 
/co rollback (/co rb) This is the standard rollback command and the one you will use when fixing simple griefs.

Following arguments available:

* user:<player> --- Use this argument to specify the player activity you want to revert.

* time:<time([s]econd,[m]inute,[h]our,[d]ay)>              Use this argument to specify how long ago you want to revert the activity back to.

* radius:<radius in blocks> Use this to specify the size of the area you want to revert activity back to.

* action:<action> Use to restrict your command to search for a specific action--- Ex:

* block >> blocks placed/broken

* +block >> blocks placed

* -block >> blocks broken

* container >> items put/taken from a chest/furnace/shulker/etc.

* +container >> items put in a chest/furnace/shulker/etc.

* -container >> items taken from a chest/furnace/shulker/etc.
 
Full Command E.g.

* /co rb time:1h user:Rossyair action:-container -- Will add the items taken, back into the chest
* /co rb time:1h user:Rossyair action:+container -- Will remove the items placed into the chest.
*Before using the interactive block rollback commands, make sure you practice with a SeniorMod or Admin first to ensure a player doesn't lose their items because of a mistake YOU made.
 
/co lookup (/co l) This command allows you to look up player activity regarding the following arguments:

* kill >> mobs/animals killed

* chat >> messages sent in chat

* command >> commands used

* session >> player logins/logouts

* +session >> player logins

* -session >> player logouts

* blocks: <block> Use to restrict your command to search for a specific block in a lookup/rollback

* exclude:<block> Use to exclude a specific block from your command in a lookup/rollback
 
Other Commands:

* /co restore (/co rs) Use this to restore a rollback.
* /co undo Use this to revert a rollback.
 
**Bans:** As you are aware from your SeniorAmbassador training, banning is the last resort and we rather give someone 17 warnings than ban them on the spot. When you have suspicions about a player, whether it's hacks, stealing, griefing, etc., get as much information as you can about them using the tools provided to you to ensure you're not framing an innocent player as a troublemaker. If you aren't sure if you should ban a player or if you're not piecing the information together correctly, never hesitate to ask another staff member for help. The Chain of Command still applies to you as a Moderator. If you have done some investigating and the things you've found strengthens your sense of question, then you are provided with:

* /jail <username> jail1 Will put a player in a confined space where they don't have access to any command but local chat.

* /unjail <username> Unjails a player. ALWAYS remember to unjail after talking to a player otherwise they will be denied the use of any commands. 

*Jail is the ideal place to question a player about suspicious information you have found about them. 

Always be as professional as you can when you are questioning a player, we DO NOT want to give the impression that we are trolling them or giving them unfair treatment. If it does come to the point that you need to ban a player, you now have access to:

*   /bane <username> <reason> command. This is the short version of the long,

*  /ban <username> <reason> < Banning Staff > Appeal at bit.ly/MWAppeal

 *Bane will automatically sign your name and provide them with the appeal link. Always sign your bans if you use the long command. 

After your first ban, you will eventually be given mod status on the forums so you can access ban appeals. You will be trained on how to deal with/respond to those when the time comes.

**Grief/WorldGuard Notifications:** As you may notice, whenever a player breaks a certain variant of block, you are notified of it. Whenever this happens, the player should be checked up on. Use the commands allotted to you to assist you in this investigation--/v & /gm 3 are your go-to's for checking if someone is griefing. Once you have confirmed that a player isn't griefing, post that in your designated in-game mod channel so the other mods know the player doesn't need to be dealt with and there is only one mod having to check up on the player rather than all the online mods doing more work than they have to because they aren't communicating. If the player DOES happen to be griefing, follow normal procedure. You can either give them a warning in local and hope they see your message, or you can ensure a listener by using the /jail <username> jail1 command. These notifications may get annoying at times, but is very efficient in detecting possible griefers.
 
**WorldEdit:** WorldEdit is a tool you will become very familiar with during your time as Mod. You will use it to assist you in building, investigate player situations, and bypass WorldGuard and player claims when necessary. Some things to keep in mind about world edit: NEVER do world edit for players. If you do world edit for one, then you will have to do it for all of them and that's how you get demoted. Also be sure to keep in mind that the higher up you do world edit, the more light blocks that will need to be loaded, the more lag. Even a WorldEdited 20x20 platform 150 blocks above the ground can lag as much or more than taking out 6000 blocks from the ground. WorldEdit allows you to alter unlimited amounts of blocks with one command, if you aren't sure where you set your <//pos1> and <//pos2> DO NOT issue a world edit command. You can easily wipe out a large chunk of the world and possibly crash the server if you aren't careful. As tedious as it may be, use your WorldEdit permissions with great caution. You should have been trained on basic commands when you were first promoted, if not, find someone to assist you as we don't want you using commands you don't know about. WorldEdit Reference: http://bit.ly/2JbfK1V
 
**Lockette:** Lockette refers to the signs that players put on chests to lock up their items so that no one can access them. In order for a player to create one of these Lockette signs, after placing a sign on their chest, they need to manually type [Private] on the first line and their username on the second line. This plugin is case sensitive, meaning that if a player doesn't include a capital in their name where it should be, or puts their name on the third line instead of the second, they will either lock themselves out of their chests or deny themselves the ability to destroy it. When this does happen, a player will need your help to reclaim access to their items. Before removing a Lockette sign, ALWAYS ALWAYS ALWAYS confirm that the chest actually belongs to them FIRST using CoreProtect. You can then remove the sign(s) using WorldEdit (//set 0) OR roll back the sign placement using CoreProtect.
 
**Deleting Player Claims:** As you may or may not know from being a player, whenever you place a chest for the first time, a small claim is formed around that chest, so players may accidentally claim over another's build unintentionally. This isn't a rare occurrence and you have the power to deal with it.  If a player complains or files a modreq about someone having claimed over their land/items, confirm that the possessions are actually theirs using CoreProtect, and then you can issue the </abandonclaim> command. It is the same command that you would use to remove your own claim, except it works for every claim. Use this with caution.
 
**Discord/BugReports:** As an Ambassador, you were provided with the 'Ambassador' role on our discord considering discord is what we use as our staff communication medium. This still holds true for Moderators--but now, you are provided with a higher rank on the discord. You now have permissions on the discord to remove messages, kick, ban, mute, etc. You are highly encouraged to moderate chat and behavior on the discord as well as the server with these new permissions. You would deal with them the same way you would on the server. With these new rule enforcement permissions, you are also provided with access to a higher authority chat channel to communicate with fellow moderators and to post about any questions or concerns, just like you would as an Ambassador. You will often be the first person a player goes to when they want to report a glitch or bug they found regarding game play, and you are required to deal with it--not fix, deal with. Gather as much information on the subject as you can before submitting the issue to an Admin in your respective discord chat channel according to the server in which you are reporting for. Include the player who reported the issue, what the issue is, when it happened, try and find out if it happened to anyone else, etc. Your goal is to make it as easy as possible for an Admin to resolve the issue at hand--there is no reason they should be investigating the basics of a presented issue when they could be figuring out fixes.

**Other Commands:**

* </back> Returns you to your previous location.

* /tp <player1> <player2> Teleports one player to another. Use if necessary, otherwise players will see you as a personal taxi.